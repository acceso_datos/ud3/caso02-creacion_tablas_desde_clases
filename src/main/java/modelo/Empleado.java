/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "empleado", catalog = "bdtareas")
public class Empleado implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "nombre", nullable = false, length = 300)
	private String nombre;
	@Column(name = "passwd", nullable = false, length = 50)
	private String passwd;

	@Column(name = "correo", unique = true, nullable = false, length = 50)
	private String email;

	@OneToMany(mappedBy = "empleado")
	private Set<Tarea> tareas = new HashSet<Tarea>(0);

	public Empleado() {
	}

	public Empleado(String nombre, String passwd, String email) {
		this.nombre = nombre;
		this.passwd = passwd;
		this.email = email;
	}

	public Empleado(String nombre, String passwd, String email, Set<Tarea> tareas) {
		this.nombre = nombre;
		this.passwd = passwd;
		this.email = email;
		this.tareas = tareas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Tarea> getTareas() {
		return tareas;
	}

	public void setTareas(Set<Tarea> tareas) {
		this.tareas = tareas;
	}

	public void addTarea(Tarea t) {
		tareas.add(t);
	}

	public void removeTarea(Tarea t) {
		tareas.remove(t);
	}

	@Override
	public String toString() {
		return "Empleado{" + "id=" + id + ", nombre=" + nombre + ", passwd=" + passwd + ", email=" + email + '}';
	}

}

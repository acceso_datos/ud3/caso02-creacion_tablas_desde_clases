package app;

import java.util.Date;

import jakarta.persistence.EntityManager;
import modelo.Empleado;
import modelo.Tarea;

public class MainApp {

	public static void main(String[] args) {
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.SEVERE);

		System.out.println("APP - GESTIÓN TAREAS");
		
		try {
//			Session orm = ConexionHB.getSession();
			EntityManager orm = ConexionEM.getEntityManager();
			
			// Creamos empleados y los guardamos en la bd
			Empleado empleado1 = new Empleado("Juan", "1234", "juan@batoi.es");
			Empleado empleado2 = new Empleado("Pepe", "1234", "pep@batoi.es");
			orm.getTransaction().begin();
			orm.persist(empleado1);
			orm.persist(empleado2);
			orm.getTransaction().commit();
			System.out.println("\tInsertado empleado " + empleado1.getId());
			System.out.println("\tInsertado empleado " + empleado2.getId());

			// Creamos tarea y asociamos el empleado a dicha tarea
			// Esto hará que automáticamente el empleado disponga de la tarea en su lista.
			Tarea t1 = new Tarea("tarea1", new Date(), null, empleado1);
			Tarea t2 = new Tarea("tarea2", new Date(), null, empleado1);
			Tarea t3 = new Tarea("tarea3", new Date(), null, empleado2);
			orm.getTransaction().begin();;
			orm.persist(t1);
			orm.persist(t2);
			orm.persist(t3);
			orm.getTransaction().commit();
			System.out.println("\tInsertadas tareas " + t1.getDescripcion() + " y " + t2.getDescripcion());

			// Recargamos datos empleados
			orm.refresh(empleado1);
//			sesion.refresh(empleado2);
			
			// Recorremos tareas del empleado1			
			System.out.println("\tLista de tareas del empleado " + empleado1);
			for (Tarea ta : empleado1.getTareas()) {
				System.out.println("\t\t" + ta);
			}			

//			ConexionHB.closeSession();
			ConexionEM.closeEntityManager();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}
